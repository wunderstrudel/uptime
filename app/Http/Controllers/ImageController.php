<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{	
	public function read() {
        return Image::all();
    }

    public function readSingle(Request $request, $id) {
        $image = Image::find($id);
    	if(isset($image) == false) {
    		return response(["errors" => "Could not find the given image."], 200);
    	}

    	
    	return response()->file($image->path);
    }

    public function create(Request $request) {
        $request->validate([
    		"title" => "required|string",
    		"image" => "required|image"
    	]);

    	$original = $request->file("image")->getClientOriginalNAme();
    	$filename = pathinfo($original, PATHINFO_FILENAME);
    	$extension = $request->file("image")->getClientOriginalExtension();
    	$file = $filename . "." . $extension;
    	$request->file("image")->storeAs("public/images", $file);
    	$path = 'storage/images/' . $file;
    	
    	$data = Image::create([
    		"title" => $request->title,
    		"path" => $path
    	]);
    	

    	return response(["success" => "Image uploaded."], 201);
    }

    public function delete(Request $request, $id) {
    	$product = Image::find($id);
    	if(isset($product) == false) {
    		return response(["errors" => "Could not find the given product."], 200);
    	}
    	$product->delete();
    	return response(["success" => "Image deleted."], 200);
    }
}
