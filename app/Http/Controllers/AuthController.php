<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{	
    /*
        DETTE ER EN HACK.
        Det ville blive en for besværlig opsætning, hvis i skulle ind i db'en og finde info'en.
        Derfor lavede jeg et hack, som gør det lidt nemmere for jer, men ville lige pointere at det er "dårlig stil". 
        So dont try this at home!
    */
    public function secret(Request $request) {
        $client = DB::select('select * from oauth_clients where name=?', ["Laravel Password Grant Client"]);
        if(sizeof($client) <= 0) {
            return response()->json('Could not get oauth client');
        }

        return response()->json(array(
            "id" => $client[0]->id,
            "client_secret" => $client[0]->secret
        ));
    }

    public function logout() {
        auth()->user()->tokens->each(function($token, $key) {
            $token->delete();
        });

        return response()->json(array("success" => "User logged out."), 200);
    }
}
