<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function read() {
        return Product::all();
    }

    public function readSingle(Request $request, $id) {
        $product = Product::find($id);
        if(isset($product) == false) {
            return response(["errors" => "Could not find the given product."], 200);
        }

        
        return response()->json($product);
    }

    public function create(Request $request) {
        $request->validate([
    		"title" => "required|string",
    		"image" => "string",
    		"description" => "string",
    		"pricing" => "numeric"
    	]);

    	

    	$data = Product::create([
    		"title" => $request->title,
    		"image" => $request->image,
    		"description" => $request->description,
    		"pricing" => $request->pricing
    	]);

    	return response(["success" => "Product added."], 201);
    }

    public function update(Request $request, $id) {
    	$request->validate([
    		"title" => "required|string",
    		"image" => "string",
    		"description" => "string",
    		"pricing" => "numeric"
    	]);

    	$product = Product::find($id);
    	if(isset($product) == false) {
    		return response(["errors" => "Could not find the given product."], 200);
    	}

    	$product->title = $request->title;
    	$product->image = $request->image;
    	$product->description = $request->description;
    	$product->pricing = $request->pricing;
    	$product->save();

    	return response(["success" => "Product updated."], 200);
    }

    public function delete(Request $request, $id) {
    	$product = Product::find($id);
    	if(isset($product) == false) {
    		return response(["errors" => "Could not find the given product."], 200);
    	}
    	$product->delete();
    	return response(["success" => "Product deleted."], 200);
    }
}
