import Vue from 'vue';
import Vuex from 'vuex';

window.Vue = require('vue');
window.Vue.use(Vuex);

export const store = new Vuex.Store({
	state: {
		token: localStorage.getItem("access_token") || null,
		categories: [],
		activeProduct: null,
		products: [],
		images: []
	},
	mutations: {
		setToken: function(state, token) {
			state.token = token;
		},
		deleteToken: function(state) {
			state.token = null;
		},
		setCategories: function(state, categories) {
			state.categories = categories;
		},
		setProducts: function(state, products) {
			state.products = products;
		},
		setImages: function(state, images) {
			state.images = images;
		},
		setActiveProduct: function(state, id) {
			state.activeProduct = id;
		},
	},
	actions: {
		getToken: function(context, data) {
			return new Promise((resolve, reject) => {
				/*
					DETTE ER EN HACK.
					Det ville blive en for besværlig opsætning, hvis i skulle ind i db'en og finde info'en.
					Derfor lavede jeg et hack, som gør det lidt nemmere for jer, men ville lige pointere at det er "dårlig stil". 
					So dont try this at home!
				*/
				axios.post('api/secret').then(response => {
					let auth = response.data;
					axios.post('oauth/token', {
						grant_type: 'password',
                    	client_id: auth.id,
                    	client_secret: auth.client_secret,
						username: data.username,
						password: data.password,
					}).then(response => {
						let token = response.data.access_token || null;
						localStorage.setItem("access_token", token);
						context.commit("setToken", token);
						resolve(response);
					}).catch(error => {
						reject(error)
					});
				}).catch(error => {
					alert("Login error.");
				});
			});
		},
		deleteToken: function(context) {
			axios.defaults.headers.common["Authorization"] = "Bearer " + context.state.token;
			if(context.getters.loggedIn) {
				return new Promise((resolve, reject) => {
					axios.post('api/logout').then(response => {
						localStorage.removeItem("access_token");
						context.commit("deleteToken");
						resolve(response);
					}).catch(error => {
						localStorage.removeItem("access_token");
						context.commit("deleteToken");
						reject(error)
					});
				});
			}
		},
		getProducts: function(context, data) {
			return new Promise((resolve, reject) => {
				axios.get('api/products', {}).then(response => {
					resolve(response);
				}).catch(error => {
					reject(error)
				});
			});
		},
		getProduct: function(context, data) {
			return new Promise((resolve, reject) => {
				axios.get('api/product/' + data.id, {}).then(response => {
					resolve(response);
				}).catch(error => {
					reject(error)
				});
			});
		},
		editProduct: function(context, data) {
			axios.defaults.headers.common["Authorization"] = "Bearer " + context.state.token;
			
			return new Promise((resolve, reject) => {
				axios.put('api/product/' + data.id, {
					title: data.title,
					image: data.image,
					description: data.description,
					pricing: data.pricing
				}).then(response => {
					resolve(response);
				}).catch(error => {
					reject(error)
				});
			});
		},
		addProduct: function(context, data) {
			axios.defaults.headers.common["Authorization"] = "Bearer " + context.state.token;

			return new Promise((resolve, reject) => {
				axios.post('api/products', {
					title: data.title,
					image: data.image,
					description: data.description,
					pricing: data.pricing
				}).then(response => {
					resolve(response);
				}).catch(error => {
					reject(error)
				});
			});
		},
		deleteProduct: function(context, data) {
			axios.defaults.headers.common["Authorization"] = "Bearer " + context.state.token;

			return new Promise((resolve, reject) => {
				axios.delete('api/product/' + data.id, {}).then(response => {
					resolve(response);
				}).catch(error => {
					reject(error)
				});
			});
		},
		getImages: function(context, data) {
			return new Promise((resolve, reject) => {
				axios.get('api/images', {}).then(response => {
					resolve(response);
				}).catch(error => {
					reject(error)
				});
			});
		},
		addImage: function(context, data) {
			axios.defaults.headers.common["Authorization"] = "Bearer " + context.state.token;

			let formData = new FormData();
			formData.append('title', data.title);
			formData.append('image', data.image);

			return new Promise((resolve, reject) => {
				axios.post('api/images', formData, {
					headers: {
					    'Content-Type': 'multipart/form-data'
					}
				}).then(response => {
					resolve(response);
				}).catch(error => {
					reject(error)
				});
			});
		},
		deleteImage: function(context, data) {
			axios.defaults.headers.common["Authorization"] = "Bearer " + context.state.token;

			return new Promise((resolve, reject) => {
				axios.delete('api/images/' + data.id, {}).then(response => {
					resolve(response);
				}).catch(error => {
					reject(error)
				});
			});
		}
	},
	getters: {
		loggedIn: function(state) {
			return state.token !== null;
		},
		activeCategory: function(state) {
			return state.activeCategory !== null;
		}
	}
});