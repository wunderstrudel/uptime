
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import VueRouter from 'vue-router';
import Vuex from 'vuex';

window.Vue = require('vue');
window.Vue.use(VueRouter);
window.Vue.use(Vuex);






/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

import {router} from './router.js';
import {store} from './store/store.js';


Vue.component('master', require('./components/Master.vue'));
Vue.component('navigation', require('./components/layouts/Navigation.vue'));
Vue.component('page', require('./components/layouts/Page.vue'));




// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if(!store.getters.loggedIn) {
      next({
        name: 'login'
      })
    } else {
      next();
    }
  }else if (to.matched.some(record => record.meta.requiresVisitor)) {
    if(store.getters.loggedIn) {
      next({
        name: 'home'
      })
    } else {
      next();
    }
  }else {
    next();
  }
});

const app = new Vue({
    el: '#app',
    router: router,
    store: store
});
