import VueRouter from 'vue-router';
window.Vue = require('vue');
window.Vue.use(VueRouter);

import Products from './components/layouts/Products.vue';
import Images from './components/layouts/Images.vue';
import Add from './components/layouts/Add.vue';
import Edit from './components/layouts/Edit.vue';
import Product from './components/layouts/Product.vue';
import Login from './components/auth/Login.vue';

const routes = [
	{
    path: "/",
    name: "home",
    component: Products,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: "/images",
    name: "images",
    component: Images,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/add",
    name: "add",
    component: Add,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/edit/:id",
    name: "edit",
    component: Edit,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/product/:id",
    name: "product",
    component: Product,
    meta: {
      requiresAuth: false
    }
  },
];

export const router = new VueRouter({
	routes: routes
});