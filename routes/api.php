<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function() {
	Route::get('/user', function (Request $request) {
	    return $request->user();
	});

	// Auth products
	Route::post('products', 'ProductController@create');
	Route::put('product/{id}', 'ProductController@update');
	Route::delete('product/{id}', 'ProductController@delete');

	// Auth images
	Route::post('images', 'ImageController@create');
	Route::delete('images/{id}', 'ImageController@delete');

	// Auth Users
	Route::post("/logout", "AuthController@logout");
});

// products
Route::get('products', 'ProductController@read');
Route::get('product/{id}', 'ProductController@readSingle');

// images
Route::get('images/', 'ImageController@read');
Route::get('images/{id}', 'ImageController@readSingle');
Route::get('images/thumb/{id}{width}', 'ImageController@thumb'); // Nåde det aldrig :(

// Users
Route::post("/secret", "AuthController@secret");